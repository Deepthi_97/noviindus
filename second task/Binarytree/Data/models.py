from django.db import models
from django.db.models.deletion import CASCADE

class parent(models.Model):
    parent_name=models.CharField(max_length=50,null=True)
    parent_id=models.IntegerField(null=True)

    def __str__(self):
        return self.parent_name

class child(models.Model):
    child_name=models.CharField(max_length=50,null=True)
    child_id=models.IntegerField(null=True)
    parent_id=models.ForeignKey(parent, on_delete=CASCADE,null=True)
    

    def __str__(self):
        return self.child_name




