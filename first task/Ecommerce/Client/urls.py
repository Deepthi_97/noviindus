from django.conf import settings
from django.urls import path
from django.conf.urls.static import static
from Client import views

urlpatterns = [
    path('signup', views.UserRegister,name='signup'),
    path('', views.UserLogin,name='login'),
    path('dash',views.dashboard,name='dashboard')
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root = settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
