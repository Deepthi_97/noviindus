from django.shortcuts import render,redirect
from Client.models import User
from django.contrib.auth import logout,authenticate,login
from django.core.files.storage import FileSystemStorage
import datetime


def UserRegister(request):
    if request.method=="POST":
        na=request.POST.get("name")
        em=request.POST.get("email")
        mo=request.POST.get("mob")
        psw=request.POST.get("pswd")
        User.objects.create_user(username=na,email=em,phone=mo,password=psw)
        return redirect ('login')
    return render (request,"signup.html")


def UserLogin(request):
    if request.method=="POST":
        em=request.POST.get("email")
        psw=request.POST.get("pswd")
        user=authenticate(request,email=em,password=psw)
        if user:
            login(request,user)
            return redirect("dashboard")
        else:
            return redirect("login")
    return render (request,"login.html")

def dashboard(request):
    return render(request,'index.html')



