from django.db import models
from django.db.models.deletion import CASCADE

class category(models.Model):
    cat_name=models.CharField(max_length=50,null=True)
    cat_image=models.ImageField(upload_to='images/',null=True)

    def __str__(self):
        return self.cat_name

class product(models.Model):
    product_name=models.CharField(max_length=50,null=True)
    product_id=models.IntegerField(null=True)
    product_image1=models.ImageField(upload_to='images/',null=True)
    product_image2=models.ImageField(upload_to='images/',null=True)
    product_image3=models.ImageField(upload_to='images/',null=True)
    product_description=models.CharField(max_length=40,null=True)
    price=models.IntegerField(null=True)
    stock=models.IntegerField(null=True)
    pcat=models.ForeignKey(category, on_delete=CASCADE,null=True)
    stock_status=models.CharField(max_length=40,null=True)

    def __str__(self):
        return self.product_name

class cart(models.Model):
    product_id=models.ForeignKey(product, on_delete=CASCADE,null=True)
    product_qty=models.IntegerField(null=True)
    user_id=models.EmailField(max_length=40,null=True)
    total=models.IntegerField(null=True)
