from django.shortcuts import render,redirect
from products.models import category,product,cart
from django.core.files.storage import FileSystemStorage
from django.db.models.fields import files
from django.http.response import HttpResponse
import os
from pathlib import Path

def addpro(request):
    obj=category.objects.all()
    if request.method=="POST":
        na=request.POST.get("product_name")
        im1=request.FILES['product_image1']
        im2=request.FILES['product_image2']
        im3=request.FILES['product_image3']
        f=FileSystemStorage()
        fp1=f.save(im1.name,im1)
        fp2=f.save(im2.name,im2)
        fp3=f.save(im3.name,im3)
        pd=request.POST.get("product_description")
        pr=request.POST.get("price")
        st=request.POST.get("stock")
        ca=request.POST.get("category")
        c=category.objects.get(id=ca)
        product.objects.create(product_name=na,product_image1=fp1,product_image2=fp2,product_image3=fp3,product_description=pd,price=pr,stock=st,pcat=c)
        return redirect('dispro')
    return render(request,"addpro.html",{'cat':obj})

def dispro(request):
    obj=product.objects.all()
    return render(request,"dispro.html",{"datas":obj})

def updpro(request,product_id):
    up=product.objects.filter(id=product_id)
    ca=category.objects.all()
    if request.method=='POST':
        na=request.POST.get("product_name")
        im1=request.FILES['product_image1']
        im2=request.FILES['product_image2']
        im3=request.FILES['product_image3']
        f=FileSystemStorage()
        fp1=f.save(im1.name,im1)
        fp2=f.save(im2.name,im2)
        fp3=f.save(im3.name,im3)
        pd=request.POST.get("product_description")
        pr=request.POST.get("price")
        st=request.POST.get("stock")
        ca=request.POST.get("category")
        c=category.objects.get(id=ca)
        up.update(product_name=na,product_image1=fp1,product_image2=fp2,product_image3=fp3,product_description=pd,price=pr,stock=st,pcat=c)
        return redirect('dispro')
    return render(request,"updpro.html",{'upd':up[0],'id':product_id,'cat':ca})

def delpro(request,product_id):
    g=product.objects.get(id=product_id)
    g.delete()
    return redirect('dispro')

def cartpro(request,product_id):
        p=product.objects.all()
        pro=product.objects.get(id=product_id)
        u=request.user.email
        pr=pro.price
        cart.objects.create(product_id=pro,product_qty=1,user_id=u,total=pr)
        return redirect('discart')

def discart(request):
    user=request.user.email
    prod=cart.objects.filter(user_id=user)
    car=cart.objects.filter(user_id=request.user.email)
    ca=category.objects.all()
    sum=0
    for i in prod:
        sum=sum+int(i.total)
    q=0
    for j in prod:
        q=q+int(j.product_qty)   
    
    return render(request,'discart.html',{'pro':prod,'cat':ca,"subt":sum,'cart':car,'qty':q})

def delcart(request,cart_id):
    c=cart.objects.get(id=cart_id)
    c.delete()
    return redirect("discart")

def updcart(request,cart_id):
    obj=cart.objects.filter(id=cart_id).values()
    v=cart.objects.get(id=cart_id)
    if request.method=="POST":
        qty=request.POST.get("qty")
    obj.update(product_qty=qty,total=int(qty)*int(v.product_id.price))
    return redirect("discart")