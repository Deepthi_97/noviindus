from django.conf import settings
from django.urls import path
from django.conf.urls.static import static
from products import views

urlpatterns = [
    path('addpro', views.addpro,name='addpro'),
    path('dispro',views.dispro,name='dispro'),
    path('delpro<int:product_id>',views.delpro,name='delpro'),
    path('updpro<int:product_id>',views.updpro,name='updpro'),
    path('cartpro<int:product_id>',views.cartpro,name='cartpro'),
    path('discart',views.discart,name='discart'),
    path('delcart<int:cart_id>',views.delcart,name='delcart'),
    path('updcart<int:cart_id>',views.updcart,name='updcart'),
    
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root = settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)